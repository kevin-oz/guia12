/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sv.edu.uesocc.ingenieria.prn335_2018.web.rest.server;

import java.util.HashSet;
import java.util.Set;
import javax.ws.rs.core.Application;

/**
 *
 * @author kevin Figueroa
 */
@javax.ws.rs.ApplicationPath("rest")
public class Config extends Application{

    @Override
    public Set<Class<?>> getClasses() {
        Set<Class<?>> resource= new HashSet<>();
        resource.add(sv.edu.uesocc.ingenieria.prn335_2018.web.rest.server.TipoEstadoReservaResource.class);
        return resource;
    }
    
    
}
